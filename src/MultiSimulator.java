import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import uk.ac.nott.cs.g54dia.multilibrary.*;



public class MultiSimulator {


	private static int DELAY = 0;
	public static int FLEET_SIZE = 3;
	private static int DURATION = 10 * 10000;
	
	private static InputStreamReader ir = new InputStreamReader(System.in);
	private static BufferedReader br = new BufferedReader(ir);
	
	public static void main(String[] args) {
		
		//For trapping fatal errors
		Boolean fatalError = false;
		
		// Create an environment
        Environment env = new Environment((Tanker.MAX_FUEL/2)-5);
                        
        //Create a fleet
        Fleet fleet = new Fleet();

        Tanker t;

        for (int i=0; i<FLEET_SIZE; i++) {
        	t = new IronTanker(i);
            fleet.add(t);
        }
        
        // Create a GUI window to show our tanker
        TankerViewer tv = new TankerViewer(fleet);
        tv.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        // Start executing the Tanker
        while (env.getTimestep()<(DURATION)) {
            // Advance the environment timestep
            env.tick();
            // Update the GUI
            tv.tick(env);
            
            for (Tanker tank:fleet) {
                // Get the current view of the tanker
                Cell[][] view = env.getView(tank.getPosition(), Tanker.VIEW_RANGE);
                // Let the tanker choose an action
                Action a = tank.senseAndAct(view, env.getTimestep());
                // Try to execute the action
                try {
                    a.execute(env, tank);
                } catch (OutOfFuelException dte) {
                	IronTanker IT = (IronTanker)tank;
                    System.err.println("Tanker " + IT.ID + " out of fuel!");
                    System.err.println("Fuel:" + IT.getFuelLevel() + ", Water: " + IT.getWaterLevel());
                    fatalError = true;
                    break;
                } catch (ActionFailedException afe) {
                    System.err.println("Failed: " + afe.getMessage());
                	MultiSimulator.pause();
                }
            }
            
            if (fatalError) {
            	break;
            }
            
            try { Thread.sleep(DELAY);} catch (Exception e) { }
            
            
        }
    }
	
	
	public static void pause() {
		System.out.println("Simulator paused!");
		while (true) {
			try {
				if (!MultiSimulator.br.readLine().equals(null)) break;
			} catch (IOException ioe) {
				System.out.println("Failed to pause");
				ioe.printStackTrace();
			}
		}
	}
	
}
