import java.math.BigInteger;

import uk.ac.nott.cs.g54dia.multilibrary.*;


public class AgentNumberExperiment {
	
	private static final int NUMBER_OF_AGENTS = 8;
	private static final int NUMBER_OF_RUNS = 10;
	
	public static void main(String[] args) {
		int[] seeds = new int[AgentNumberExperiment.NUMBER_OF_RUNS];
		Long[] results = new Long[AgentNumberExperiment.NUMBER_OF_RUNS];
		
		for (int i = 0; i < seeds.length; i++) {
			seeds[i] = (int) Math.random();
		}
		
		for (int agents = 2; agents <= AgentNumberExperiment.NUMBER_OF_AGENTS; agents++) {
			System.out.println("Number of agents: " + agents);
			for (int i = 1; i <= AgentNumberExperiment.NUMBER_OF_RUNS; i++) {
				Simulator sim = new Simulator(2, seeds[i]);
				results[i] = sim.run();
			}
			
			BigInteger sum = new BigInteger("0");
			for (int i = 0; i < AgentNumberExperiment.NUMBER_OF_RUNS; i++) {
				System.out.println("Run " + i + ": " + results[i]);
				sum.add(new BigInteger(results[i].toString()));
			}
			System.out.println("Average for " + agents + " agents: " + sum.divide(new BigInteger("10")));
		}
	}
	
}

class Simulator {
	public int FLEET_SIZE = 0;
	private int seed = 0;
	private static int DURATION = 10 * 10000;
	
	public Simulator(int fleetSize, int seed) {
		this.FLEET_SIZE = fleetSize;
		this.seed = seed;
	}
	
	public long run() {
		RandomGenerator.getInstance().setSeed(this.seed);
		
		//For trapping fatal errors
		Boolean fatalError = false;
		
		// Create an environment
	    Environment env = new Environment((Tanker.MAX_FUEL/2)-5);
	                    
	    //Create a fleet
	    Fleet fleet = new Fleet();
	
	    Tanker t;
	
	    for (int i=0; i<FLEET_SIZE; i++) {
	    	t = new IronTanker(i);
	        fleet.add(t);
	    }
	    
	    // Create a GUI window to show our tanker
	    TankerViewer tv = new TankerViewer(fleet);
	    tv.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
	    // Start executing the Tanker
	    while (env.getTimestep()<(DURATION)) {
	        // Advance the environment timestep
	        env.tick();
	        // Update the GUI
	        tv.tick(env);
	        
	        for (Tanker tank:fleet) {
	            // Get the current view of the tanker
	            Cell[][] view = env.getView(tank.getPosition(), Tanker.VIEW_RANGE);
	            // Let the tanker choose an action
	            Action a = tank.senseAndAct(view, env.getTimestep());
	            // Try to execute the action
	            try {
	                a.execute(env, tank);
	            } catch (OutOfFuelException dte) {
	            	IronTanker IT = (IronTanker)tank;
	                System.err.println("Tanker " + IT.ID + " out of fuel!");
	                System.err.println("Fuel:" + IT.getFuelLevel() + ", Water: " + IT.getWaterLevel());
	                fatalError = true;
	                break;
	            } catch (ActionFailedException afe) {
	                System.err.println("Failed: " + afe.getMessage());
	            	MultiSimulator.pause();
	            }
	        }
	        
	        if (fatalError) {
	        	break;
	        }
	    }
	    
	    return fleet.getScore();
	}
}
