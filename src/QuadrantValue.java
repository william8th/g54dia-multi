import java.util.AbstractMap.SimpleEntry;


public class QuadrantValue extends SimpleEntry<Integer, Integer> implements Comparable<QuadrantValue> {

	private static final long serialVersionUID = -7227605433542177030L;

	/**
	 * A pair to represent a quadrant and its density
	 * @param arg0 Quadrant index
	 * @param arg1 Quadrant density
	 */
	public QuadrantValue(Integer arg0, Integer arg1) {
		super(arg0, arg1);
	}

	@Override
	public int compareTo(QuadrantValue o) {
		return this.getValue() - o.getValue();
	}

}
