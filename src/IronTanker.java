import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import uk.ac.nott.cs.g54dia.multilibrary.*;

public class IronTanker extends Tanker {

	private final static int 
		TANKER_STATE_FORAGE = 10,
		TANKER_STATE_EXECUTE = 20;
	private final static MPoint ORIGIN = new MPoint(0, 0);
	
	public int ID = -1;  // ID of the tanker
	private int bx = 0;
	private int by = 0;
	private int direction = 0;
	private int state = IronTanker.TANKER_STATE_FORAGE;
	private MPoint currentPos = null;
	private MPoint closestForageStation = null;
	private ArrayList<MPoint> foragedStations = new ArrayList<>();
	
	// Wells and stations are shared. These are used to keep track of everything seen.
	private static ArrayList<Node> wells = new ArrayList<>();
	private static ArrayList<Node> stations = new ArrayList<>();
	private static ArrayList<QuadrantValue> quadrantDensity = new ArrayList<>();  // Used for quadrant analysis
	
	private static ContractNetProtocol cnp = new ContractNetProtocol();
	private ArrayList<Job> myJobs = new ArrayList<>();
	private Plan currentPlan = null;
	private ArrayList<Node> steps = null;
	
	
	
	/******************************
	 * Needed to simplify arithmetics in foraging
	 ******************************/
	private final static int
		NORTH = 0,
		NORTHEAST = 1,
		EAST = 2,
		SOUTHEAST = 3,
		SOUTH = 4,
		SOUTHWEST = 5,
		WEST = 6,
		NORTHWEST = 7;
	
	

	/******************************
	 * Forage configuration START
	 ******************************/
	private final static int
		FORAGE_IDLE = 0,
		FORAGE_FIRST = 1,
		FORAGE_SECOND = 2;
	private static int[][][] foragePattern = {
		{
			{NORTH, EAST, SOUTHWEST},
			{NORTHEAST, SOUTH, WEST}
		},
		{
			{EAST, SOUTH, NORTHWEST},
			{SOUTHEAST, WEST, NORTH}
		},
		{
			{SOUTH, WEST, NORTHEAST},
			{SOUTHWEST, NORTH, EAST}
		},
		{
			{WEST, NORTH, SOUTHEAST},
			{NORTHWEST, EAST, SOUTH}
		}
	};
	private final static int targetDistance = 33;
	
	
	private int distanceCompleted = 0;
	private int patternCompleted = 0;
	private int quadrantToExplore = -1;
	private int quadrantAreaToExplore = -1;
	private int[] patternSelected = null;
	
	private int forageState = IronTanker.FORAGE_IDLE;
	
	private static boolean firstForageDone = false;
	private static boolean[] forageAreaCompleted = 
		{
			false, false, false, false,
			false, false, false, false
		};
	/******************************
	 * Forage configuration END
	 ******************************/
	
	
	
	
	
	
	/**
	 * Tanker constructor
	 * @param id The ID associated with the tanker
	 */
	public IronTanker(int id) {
		this.ID = id;
		
		// Initialise quadrants
		if (IronTanker.quadrantDensity.size() == 0) {
			for (int i = 0; i < 4; i++) {
				IronTanker.quadrantDensity.add(new QuadrantValue(i, 0));
			}
		}
	}
	
	private static void addWell(Cell c, MPoint m) {
		IronTanker.wells.add(new Node(c, m));
	}
	
	private static void addStation(Cell c, MPoint m) {
		IronTanker.stations.add(new Node(c, m));
	}
	
	/**
	 * Convert direction notation used in this tanker to the notation used by the simulator.
	 * @param myDirection The direction as defined in this class
	 * @return Converted direction as defined in MoveAction class
	 */
	private static int convertDirection(int myDirection) {
		int direction = -1;
		switch (myDirection) {
		case NORTH:
			direction = MoveAction.NORTH;
			break;
			
		case NORTHEAST:
			direction = MoveAction.NORTHEAST;
			break;
			
		case EAST:
			direction = MoveAction.EAST;
			break;
			
		case SOUTHEAST:
			direction = MoveAction.SOUTHEAST;
			break;
			
		case SOUTH:
			direction = MoveAction.SOUTH;
			break;
			
		case SOUTHWEST:
			direction = MoveAction.SOUTHWEST;
			break;
			
		case WEST:
			direction = MoveAction.WEST;
			break;
			
		case NORTHWEST:
			direction = MoveAction.NORTHWEST;
			break;
		}
		return direction;
	}
	
	/**
	 * Quadrant analysis to indicate which quadrant has the most stations so that agents can exploit this information
	 * to check for more tasks. Increment station count of the quadrant that the station is located in.
	 * @param m A station that has never been seen before
	 */
	private static void quadrantAnalysis(MPoint m) {
		int quadrantIndex = -1;
		if (m.x >= 0 && m.y >= 0) {
			if (m.y >= 0)
				quadrantIndex = 0;
			else
				quadrantIndex = 1;
		} else {
			if (m.y >= 0)
				quadrantIndex = 3;
			else
				quadrantIndex = 2;
		}
		
		for (QuadrantValue qv : IronTanker.quadrantDensity) {
			if (qv.getKey() == quadrantIndex) {
				qv.setValue(qv.getValue() + 1);
				break;
			}
		}
	}
	
	/**
	 * The overall process flow is separated into these parts:
	 * 1. Check if any bids have been won.
	 * 2. Allocate tasks that this tanker has seen (this tanker is the discoverer) to the tanker with the highest bid.
	 * 3. Analyse the area that the tanker sees. (QuadrantAnalysis goes here)
	 * 4. Come up with plan including the jobs that are owned by this tanker and jobs that have not been allocated. (Potential plan)
	 * 5. Bid for stations in the potential plan so that this tanker owns the job.
	 * 6. Come up with best plan for the jobs that this tanker currently owns.
	 * 7. Open up jobs that we own yet aren't included in the best plan. This prevents an agent from hogging a task.
	 * 8. Execute best plan if there exists one.
	 * 9. Forage if there are no plans.
	 * 10. Repeat.
	 */
	@Override
	public Action senseAndAct(Cell[][] view, long timestep) {
		this.currentPos = new MPoint(this.bx, this.by);

		// Get won bids (Section 1)
		for (Job j : IronTanker.cnp.allocatedJobs) {
			if (j.owner == this.ID && !this.myJobs.contains(j)) {
				this.myJobs.add(j);
			}
		}
		
		// Allocate tasks based on bid (Section 2)
		Job[] unallocatedJobs = IronTanker.cnp.unallocatedJobsDiscoveredBy(this.ID);
		if (unallocatedJobs != null) {
			for (Job j : unallocatedJobs) {
				Bid[] bids = IronTanker.cnp.getJobBids(j);
				if (bids != null) {
					Arrays.sort(bids, new BidComparator());
					Bid winningBid = bids[0];  // We only want the first winning bid
					
					if (winningBid != null) {
						IronTanker.cnp.awardJob(j, winningBid.bidder);
					} else {
						// No one has bidden for it yet
						continue;
					}
				}
			}
		}
		
		// Collect partially observable information from percept (Section 3)
		for (int x = 0; x < view.length; x++) {
			for (int y = 0; y < view[x].length; y++) {
				Cell c = view[x][y];
				MPoint m = new MPoint(x-Tanker.VIEW_RANGE+this.bx, Tanker.VIEW_RANGE-y+this.by);
				
				if (c instanceof Well && !IronTanker.existsCell(c, m)) {
					IronTanker.addWell(c, m);
				}
				
				if (c instanceof Station) {
					
					Station s = (Station)c;
					if (s.getTask() != null) {
						Job j = new Job(m, s, this.ID);
						
						// Broadcast job
						if (!IronTanker.cnp.existsJob(j)) {
							IronTanker.cnp.broadcastJob(j);
						}
					}
					
					// Quadrant analysis
					if (!IronTanker.existsCell(c, m)) {
						IronTanker.addStation(c, m);
						IronTanker.quadrantAnalysis(m);
					}
				}
			}
		}
		
		// Plan and bid (Section 4 and 5)
		ArrayList<Job> jobsToConsider = new ArrayList<>();
		jobsToConsider.addAll(this.myJobs);
		jobsToConsider.addAll(IronTanker.cnp.unallocatedJobs);
		
		// Try to come up with best plan to bid (Section 4 and 5)
		Plan bestPlan = this.backtrack(jobsToConsider, this.currentPos, this.getFuelLevel()-1, this.getWaterLevel(), 0, new ArrayList<MPoint>());
		if (bestPlan != null) {
			boolean toBid = true;
			if (this.currentPlan != null && this.currentPlan.score > bestPlan.score) toBid = false;
			
			if (toBid) {
				int timeToComplete = 0;
				for (Node n : bestPlan.steps) {
					timeToComplete++;
					if (n.cell instanceof Station && !this.isMyJob(n.m)) {
						// Not my job, try to bid for the job
						int score = (int)bestPlan.score;
						int distance = bestPlan.cost;
						IronTanker.cnp.bid(n.m, new Bid(score, distance, bestPlan.getNumberOfTasks(), timeToComplete, this.ID));
					}
				}
			}
		}
		
		// Come out with best plan from a list of my jobs (Section 6)
		Plan currentBestPlan = this.backtrack(this.myJobs, this.currentPos, this.getFuelLevel(), this.getWaterLevel(), 0, new ArrayList<MPoint>());
		if (this.currentPlan == null && currentBestPlan != null) {
			this.currentPlan = currentBestPlan;
			this.steps = this.currentPlan.steps;
		} else if (currentBestPlan != null) {
			long currentPlanScore = ((this.currentPlan != null) ? this.currentPlan.score : 0); 
			if (currentBestPlan.score > currentPlanScore || this.steps.size() == 1) {  // We can change the plan if the tanker is only station-foraging
				this.currentPlan = currentBestPlan;
				this.steps = this.currentPlan.steps;
			}
		}
		
		// // We try to open up jobs that we have successfully bidden for but we now have no use for it (Section 7)
		if (this.steps != null) {
			this.state = IronTanker.TANKER_STATE_EXECUTE;

			for (Job j : new ArrayList<Job>(this.myJobs)) {
				boolean isJobIncluded = false;
				for (Node n : this.steps) {
					if (n.m.equals(j.m)) {
						isJobIncluded = true;
						break;
					}
				}
				
				if (!isJobIncluded) {
					// Remove the job from local agent and open it up for bid again
					this.myJobs.remove(j);
					IronTanker.cnp.reopenJobForBidding(j);
				}
			}
		} else {
			this.state = IronTanker.TANKER_STATE_FORAGE;
		}
		
		// Execute according to tanker state (Section 8)
		int nextDirection = -1;
		if (this.state == IronTanker.TANKER_STATE_EXECUTE) {
			// Always reset forage states
			this.resetForageState();
			
			// Try to execute steps in plan
			boolean isNextTargetSet = false;
			Action specialAction = null;
			while (!isNextTargetSet) {
				Node targetNode = this.steps.get(this.steps.size()-1);
				
				// We want to forage more in hope of getting more tasks (before finishing the final step of a plan)
				if (targetNode.cell == null) {  // Null indicates FuelPump, bad practice
					if (this.getFuelLevel()-1 > Util.getInstance().absDistance(this.currentPos, IronTanker.ORIGIN)) {
						// Can continue going to the closest known station
						this.closestForageStation = this.getClosestUnvisitedStation();
						boolean isNextStationToForageSet = false;
						while (!isNextStationToForageSet) {
							try {
								nextDirection = this.moveToPoint(this.closestForageStation);
								isNextTargetSet = true;
								isNextStationToForageSet = true;
							} catch (Exception e) {
								// Already at the desired forage station, forage the next station
								this.foragedStations.add(this.closestForageStation);
								this.closestForageStation = this.getClosestUnvisitedStation();
							}
						}

						// We do not want to execute the final step which is going to the fuel pump so quickly
						continue;
					} else {
						// We have to continue with the plan's execution phase, not enough fuel to forage
						this.foragedStations.removeAll(this.foragedStations);
					}
				}
				
				// Plan step execution phase
				try {
					nextDirection = this.moveToPoint(targetNode.m);
					isNextTargetSet = true;
				} catch (Exception e) {
					
					// Already there, execute an action
					if (targetNode.cell == null && this.getFuelLevel() < Tanker.MAX_FUEL) {  // Null is used to indicate FuelPump, bad practice, I know
						specialAction = new RefuelAction();
					} else if (targetNode.cell instanceof Well && this.getWaterLevel() < Tanker.MAX_WATER) {
						specialAction = new LoadWaterAction();
					} else if (targetNode.cell instanceof Station) {
						Station s = (Station)targetNode.cell;
						
						// Remove the job as it is now complete
						Job completedJob = new Job(targetNode.m, s);
						IronTanker.cnp.jobComplete(completedJob);
						this.myJobs.remove(completedJob);
						specialAction = new DeliverWaterAction(s.getTask());
					}
					
					// Remove the step from list of steps
					this.steps.remove(this.steps.size()-1);
					
					// Check if there are still steps in the plan to execute
					if (this.steps.size() <= 0) {
						this.currentPlan = null;
						this.steps = null;
						this.state = IronTanker.TANKER_STATE_FORAGE;
						this.resetForageState();
						break;
					}
					
					if (specialAction != null) {
						isNextTargetSet = true;
					}
				}
			}
			
			if (specialAction != null) return specialAction;

		}
		
		// Try to foraage (Section 9)
		if (this.state == IronTanker.TANKER_STATE_FORAGE) {
			nextDirection = this.forage();
			if (nextDirection == -1) {
				// Refuel
				return new RefuelAction();
			}
		}
		
		this.direction = IronTanker.convertDirection(nextDirection);
		this.changeBelief(this.direction);
		return new MoveAction(direction);
	}
	
	/**
	 * Used for foraging after finishing executing a plan. Instead of going straight to the fuel pump after delivering water
	 * to all stations in the plan, this method is used to visit a station closes to the tanker's belief position.
	 * @return Coordinates of the closest unvisited station
	 */
	private MPoint getClosestUnvisitedStation() {
		MPoint closestStation = null;
		int minDistance = Tanker.MAX_FUEL;
		for (Node n : IronTanker.stations) {
			int distance = Util.getInstance().absDistance(this.currentPos, n.m);
			if (!this.foragedStations.contains(n.m) && distance < minDistance) {
				minDistance = distance;
				closestStation = n.m;
			}
		}
		return closestStation;
	}
	
	/**
	 * Reset forage configuration for a tanker
	 */
	private void resetForageState() {
		this.forageState = IronTanker.FORAGE_IDLE;
		this.distanceCompleted = 0;
		this.patternCompleted = 0;
		this.quadrantToExplore = -1;
		this.quadrantAreaToExplore = -1;
		this.patternSelected = null;
	}
	
	/**
	 * Backtracking algorithm to find for a best plan for this agent. Be aware that this function is a recursive function therefore a few of the 
	 * parameters are included for recursion purposes.
	 * @param jobs Jobs to consider to be included in the plan
	 * @param previousPos The belief position of the tanker
	 * @param availableCost The fuel available in the tanker
	 * @param availableWater The water available in the tanker
	 * @param prevWater Set to 0 at root level (default). Needed for planning to project the water level ahead.
	 * @param visited Stations that have been visited by this function.
	 * @return A plan that is considered best
	 */
	private Plan backtrack(final ArrayList<Job> jobs, final MPoint previousPos, final int availableCost, final int availableWater, final int prevWater, final ArrayList<MPoint> visited) {
		// Calculate score and cost
		ArrayList<Plan> candidatePlans = new ArrayList<>();
		
		for (Job j : jobs) {
			Plan p = new Plan();
			boolean isWellAdded = false;
			
			// Add fuel pump if we are creating a new plan
			if (previousPos.equals(this.currentPos)) {
				p.add(new Node(null, IronTanker.ORIGIN));  // All plans end with the fuel pump as its destination
			}
			
			Station s = j.s;
			if (visited.contains(j.m))
				continue;    // Skip if this station has been visited

			
			int waterRequired = s.getTask().getRequired();
			if (waterRequired + prevWater > Tanker.MAX_WATER) {
				Node bestWell = this.getBestWellFrom(previousPos);
				if (bestWell == null) return null;
				p.add(new Node(bestWell.cell, bestWell.m));
			}
			
			if (p.cost > availableCost) return null;    // If we cannot make it to the well for the previous station, discard everything
			
			p.add(new Node(s, j.m));  // Add to visit station
			if (p.cost > availableCost) return null;    // We can't make it to the next station, discard everything
			
			if (waterRequired + prevWater > availableWater) {
				// Need to add well in path
				// Select the one shortest from current position or shortest from target station
				Node bestWell = this.getBestWellFrom(j.m);
				
				// If we don't have any wells in memory, we cannot plan
				if (bestWell == null) return null;
				
				p.add(new Node(bestWell.cell, bestWell.m));
				isWellAdded = true;
			}
			
			
			MPoint lastPosInPlan = p.steps.get(p.steps.size()-1).m;
			int current2StartCost = Util.getInstance().absDistance(this.currentPos, lastPosInPlan);
			int thisPlan2PreviousPlanCost = 0;
			if (p.steps.size() > 0)
				thisPlan2PreviousPlanCost = Util.getInstance().absDistance(p.steps.get(0).m, previousPos);
			
			int calculatedCost = availableCost - p.cost - current2StartCost - thisPlan2PreviousPlanCost;
			if (calculatedCost <= 0) {
				// Reached the limits of tank
				continue;
			} else {
				visited.add(j.m);
				int accumulateWater = (isWellAdded) ? 0 : waterRequired + prevWater;
				@SuppressWarnings("unchecked")
				Plan bestChildPlan = this.backtrack(jobs, lastPosInPlan, availableCost-p.cost-thisPlan2PreviousPlanCost, availableWater-waterRequired, accumulateWater, (ArrayList<MPoint>)visited.clone());
				p.merge(bestChildPlan);
				candidatePlans.add(p);
			}
		}
		
		
		
		if (candidatePlans.size() == 0)
			return null;
		else {
			// Rank the candidate plans
			Collections.sort(candidatePlans, Collections.reverseOrder());
			return candidatePlans.get(0);
		}
	}
	
	/**
	 * Get the best well from a point of interest
	 * @param m The coordinates of the point of interest
	 * @return A Node object containing the well's instance and the absolute coordinates of the well
	 */
	private Node getBestWellFrom(MPoint m) {
		Node well2cur = this.getNearestWellFrom(this.currentPos);
		Node well2station = this.getNearestWellFrom(m);
		
		if (well2cur == null) return well2station;
		
		if (Util.getInstance().absDistance(this.currentPos, well2cur.m) < Util.getInstance().absDistance(m, well2station.m))
			return well2cur;
		else
			return well2station;
	}
	
	/**
	 * Get the nearest well from a pair of coordinates
	 * @param m The coordinate of interest
	 * @return A Node object containing the well's instance and the absolute coordinates of the well
	 */
	private Node getNearestWellFrom(MPoint m) {
		Node min = null;
		int distance = Integer.MAX_VALUE;
		for (Node n : IronTanker.wells) {
			int c2mDist = Util.getInstance().absDistance(n.m, m);
			if (c2mDist < distance) {
				distance = c2mDist;
				min = n;
			}
		}
		
		return min;
	}
	
	/**
	 * Check whether a job is allocated to this particular agent
	 * @param m The coordinates of a station with a task
	 * @return Does the job belong to this agent
	 */
	private boolean isMyJob(MPoint m) {
		boolean isMine = false;
		for (Job j : this.myJobs) {
			if (j.m.equals(m)) {
				isMine = true;
				break;
			}
		}
		return isMine;
	}
	
	/**
	 * The defined foraging pattern that is flexible for multiple agents. Note that integer returned is not defined in the MoveAction class.
	 * @return A direction to move in as defined in IronTanker class instead of the MoveAction class
	 */
	private int forage() {
		int direction = -1;
		
		switch (this.forageState) {
		case IronTanker.FORAGE_IDLE:
			boolean needAreaAssignment = true;
			this.forageState = IronTanker.FORAGE_FIRST;
			
			if ( ! IronTanker.firstForageDone) {
				boolean allComplete = true;
				int area = 0;
				for (boolean areaCompleted : IronTanker.forageAreaCompleted) {
					if (!areaCompleted) {
						this.quadrantToExplore = area / 2;
						this.quadrantAreaToExplore = area % 2;
						allComplete = false;
						break;
					}
					area++;
				}
				
				if (allComplete) {
					IronTanker.firstForageDone = true;
					IronTanker.resetForageAreaCompleted();
				} else {
					IronTanker.forageAreaCompleted[area] = true;
					needAreaAssignment = false;
				}
			}
			
			if (needAreaAssignment) {
				// Let only the first tanker forage the most dense quadrant
				Collections.sort(IronTanker.quadrantDensity);
				QuadrantValue qv = IronTanker.quadrantDensity.get(this.ID % 4);  // There are only 4 quadrants
				this.quadrantToExplore = qv.getKey();
				
				if (this.quadrantAreaToExplore == 0)
					this.quadrantAreaToExplore = 1;
				else
					this.quadrantAreaToExplore = 0;
			}
			
			break;
			
		case IronTanker.FORAGE_FIRST:
			// Do nothing, continue foraging
			break;
			
		case IronTanker.FORAGE_SECOND:
			if (this.distanceCompleted == 0) {
				if (this.quadrantToExplore == 1 || this.quadrantToExplore == 3) {
					this.quadrantAreaToExplore = (this.quadrantAreaToExplore + 1) % 2;
				}
				this.quadrantToExplore = (this.quadrantToExplore + 2) % 4;
			}
			break;
		}  // End of switch
		
		
		
		// Start foraging now that we have which quadrant the tanker wants to head towards
		if (this.patternSelected == null) {  // Select a pattern to forage
			this.patternSelected = IronTanker.foragePattern[this.quadrantToExplore][this.quadrantAreaToExplore];
		}
		
		// Tracks how far has the tanker been foraging
		if (this.distanceCompleted >= IronTanker.targetDistance) {
			this.distanceCompleted = 0;
			this.patternCompleted++;
			
			if (this.patternCompleted == 3) {
				// Finished the pattern
				this.patternCompleted = 0;
				this.patternSelected = null;
				
				// Gotta mark that first forage of the quadrant area has been done
				int areaCompleted = this.quadrantToExplore * 2 + this.quadrantAreaToExplore;
				IronTanker.forageAreaCompleted[areaCompleted] = true;
				
				// Change the forage state
				this.forageState = (this.forageState + 1) % 3;
				
				return -1;  // End prematurely to refuel
			}
		}
		
		direction = this.patternSelected[this.patternCompleted];
		this.distanceCompleted++;
		return direction;
	}
	
	/**
	 * Reset all forage configuration
	 */
	private static void resetForageAreaCompleted() {
		for (int i = 0; i < IronTanker.forageAreaCompleted.length; i++) {
			IronTanker.forageAreaCompleted[i] = false;
		}
	}
	
	/**
	 * Check if a well or station that an agent sees has been included in the belief state
	 * @param c The cell seen
	 * @param m The absolute coordinates of the cell
	 * @return Exists or not
	 */
	private static boolean existsCell(Cell c, MPoint m) {
		boolean exists = false;
		ArrayList<Node> ptr = null;
		
		if (c instanceof Well)
			ptr = IronTanker.wells;
		else
			ptr = IronTanker.stations;
		
		for (Node n : ptr) {
			if (m.equals(n.m)) {
				exists = true;
				break;
			}
		}
		return exists;
	}
	
	/**
	 * Calculate which direction to move the agent towards
	 * @param m A belief position of the target
	 * @return Direction
	 */
	private int moveToPoint(MPoint m) throws Exception {
		if (m.x == this.bx && m.y == this.by) {
			throw new Exception("Already there!");
		}
		
		int dx = m.x - this.bx;
        int dy = m.y - this.by;
        boolean north, south, east, west;
        north = south = east = west = false;
        
        if (dx < 0) {
        	west = true;
        } else if (dx > 0) {
        	east = true;
        }
        
        if (dy < 0) {
        	south = true;
        } else if (dy > 0) {
        	north = true;
        }
        
        int d = 0;
        if (north) d = IronTanker.NORTH;
        if (south) d = IronTanker.SOUTH;
        if (east) d = IronTanker.EAST;
        if (west) d = IronTanker.WEST;
        
        // We would like to rewrite the value if it wants to move diagonally
        if (north && east) d = IronTanker.NORTHEAST;
        if (north && west) d = IronTanker.NORTHWEST;
        if (south && east) d = IronTanker.SOUTHEAST;
        if (south && west) d = IronTanker.SOUTHWEST;
        
        return d;
	}
	
	
	/**
	 * Change the agent's belief state including its own position and the positions of the special cells
	 * @param direction Direction that the agent is going to move towards
	 */
	private void changeBelief(int direction) {
		switch (direction) {
		case MoveAction.NORTH:
			this.by++;
			break;
		case MoveAction.SOUTH:
			this.by--;
			break;
		case MoveAction.EAST:
			this.bx++;
			break;
		case MoveAction.WEST:
			this.bx--;
			break;
		case MoveAction.NORTHEAST:
			this.bx++;
			this.by++;
			break;
		case MoveAction.NORTHWEST:
			this.bx--;
			this.by++;
			break;
		case MoveAction.SOUTHEAST:
			this.bx++;
			this.by--;
			break;
		case MoveAction.SOUTHWEST:
			this.bx--;
			this.by--;
			break;
		}
	}

}
