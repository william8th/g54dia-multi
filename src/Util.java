
public class Util {
	private static Util instance = null;
	protected Util() {}
	
	/*
	 * A singleton
	 */
	public static Util getInstance() {
		if (instance == null) {
			instance = new Util();
		}
		return instance;
	}
	
	/*
	 * Used to calculate the distance between two points
	 */
	public int absDistance(MPoint p, MPoint q) {
		int horizontal = Math.abs(p.x - q.x);
		int vertical = Math.abs(p.y - q.y);
		
		int result = 0;
		if (horizontal > vertical)
			result = horizontal;
		else
			result = vertical;
		
		return result;
	}
}
