
public class Bid implements Comparable<Bid> {
	public int bidder = -1;
	public float metric = -1;
	
	public Bid(int score, int distance, int numberOfTasks, int timeToComplete, int bidder) {
		metric = ((long)score * (long)numberOfTasks) / ((float)distance * timeToComplete);
		this.bidder = bidder;
	}

	@Override
	public int compareTo(Bid o) {
		return (int)(o.metric - this.metric);
	}
	
	@Override
	public String toString() {
		return "(" + this.bidder + ", " + this.metric + ")";
	}
}
