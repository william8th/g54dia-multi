import java.util.Comparator;


public class BidComparator implements Comparator<Bid> {

	@Override
	public int compare(Bid b0, Bid b1) {
		if (b0 == null && b1 == null) {
            return 0;
        }
        if (b0 == null) {
            return 1;
        }
        if (b1 == null) {
            return -1;
        }
        return b0.compareTo(b1);
	}

}
