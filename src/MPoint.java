
public class MPoint {
	public int x = 0;
	public int y = 0;
	
	public MPoint(int x, int y) {
		this.x= x;
		this.y = y;
	}
	
	@Override
	public boolean equals(Object m) {
		boolean equality = false;
		
		if (m instanceof MPoint) {
			MPoint p = (MPoint)m;
			if (this.x == p.x && this.y == p.y)
				equality = true;
		}
		
		return equality;
	}
	
	@Override
	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}
}
