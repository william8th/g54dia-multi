import java.util.ArrayList;
import java.util.HashMap;


public class ContractNetProtocol {

	public ArrayList<Job> allJobs = new ArrayList<>();
	public ArrayList<Job> allocatedJobs = new ArrayList<>();
	public ArrayList<Job> unallocatedJobs = new ArrayList<>();
	public HashMap<Job, Bid[]>bids = new HashMap<>();
	
	/**
	 * Check if a job has already been discovered previously
	 * @param j The job to check for
	 * @return Has the job been discovered before
	 */
	public boolean existsJob(Job j) {
		return this.allJobs.contains(j);
	}
	
	/**
	 * Make a task announcement
	 * @param j The job to broadcast
	 */
	public void broadcastJob(Job j) {
		this.allJobs.add(j);
		this.unallocatedJobs.add(j);
		this.bids.put(j, new Bid[MultiSimulator.FLEET_SIZE]);
	}
	
	/**
	 * A tanker can try to bid for a job using this method
	 * @param m The position of the tankers
	 * @param b The bid to make
	 */
	public void bid(MPoint m, Bid b) {
		Job j = null;
		for (Job i : this.unallocatedJobs) {
			if (i.m.equals(m)) {
				j = i;
				break;
			}
		}
		
		Bid existingBid = this.bids.get(j)[b.bidder];
		if (existingBid != null) {
			// There is an existing bid, compare and replace the bid if current bid is better
			if (b.metric > existingBid.metric) {
				this.bids.get(j)[b.bidder] = b;
			}
		} else {
			this.bids.get(j)[b.bidder] = b;
		}
	}
	
	/**
	 * Get the list of jobs discovered by a tanker (used for managing purposes)
	 * @param id The ID of the tanker
	 * @return The list of jobs discovered by the tanker
	 */
	public Job[] unallocatedJobsDiscoveredBy(int id) {
		ArrayList<Job> jobs = new ArrayList<>();
		for (Job j : this.unallocatedJobs) {
			if (j.discoverer == id) {
				jobs.add(j);
			}
		}
		
		int size = jobs.size();
		Job[] jobsToReturn = null;
		if (size > 0) {
			jobsToReturn = new Job[size];
			jobsToReturn = jobs.toArray(jobsToReturn);
		}
		
		return jobsToReturn;
	}
	
	/**
	 * Tankers can reopen certain jobs, in other words, give up a job that it has won previously to other tankers.
	 * This can happen if a tanker believes it cannot complete the job or it has found a better plan to execute.
	 * @param j The job to reopen for bidding
	 */
	public void reopenJobForBidding(Job j) {
		// Get the real job from all jobs
		Job realJob = null;
		for (Job i : this.allJobs) {
			if (i.m.equals(j.m)) {
				realJob = i;
				break;
			}
		}
		
		realJob.owner = -1;
		this.allocatedJobs.remove(realJob);
		this.unallocatedJobs.add(realJob);
		this.bids.put(realJob, new Bid[MultiSimulator.FLEET_SIZE]);
	}
	
	/**
	 * Get all the bids for a job
	 * @param j The job
	 * @return A list of bids, can be a list of null objects
	 */
	public Bid[] getJobBids(Job j) {
		return this.bids.get(j);
	}
	
	/**
	 * Award a job to a tanker. This is used by the manager to award a job.
	 * @param j The job to award
	 * @param winnerID The ID of the tanker who had won the job
	 */
	public void awardJob(Job j, int winnerID) {
		this.unallocatedJobs.remove(j);
		j.owner = winnerID;
		this.allocatedJobs.add(j);
	}
	
	/**
	 * Notify saying that a job has been completed.
	 * @param j The job
	 */
	public void jobComplete(Job j) {
		this.allocatedJobs.remove(j);
		this.allJobs.remove(j);
	}
}
