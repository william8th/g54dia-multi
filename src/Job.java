import uk.ac.nott.cs.g54dia.multilibrary.Station;


public class Job {
	public Station s = null;
	public MPoint m = null;
	public int waterRequired = 0;
	public int discoverer = -1;  // Who discovered this task?
	public int owner = -1;  // Who owns this task?
	
	public Job(MPoint m, Station s, int discovererID) {
		this.s = s;
		this.m = m;
		this.waterRequired = s.getTask().getRequired();
		this.discoverer = discovererID;
	}
	
	public Job(MPoint m, Station s) {
		this.s = s;
		this.m = m;
		this.waterRequired = s.getTask().getRequired();
	}
	
	@Override
	public boolean equals(Object o) {
		boolean equals = false;
		if (o instanceof Job) {
			Job j = (Job)o;
			equals = j.m.equals(this.m) && j.waterRequired == this.waterRequired;
		}
		return equals;
	}
	
	@Override
	public String toString() {
		return this.m.toString() + " " + this.waterRequired;
	}
}
