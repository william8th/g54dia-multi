import java.util.ArrayList;

import uk.ac.nott.cs.g54dia.multilibrary.Station;


public class Plan implements Comparable<Plan> {
	public long score = 0;
	public int cost = 0;
	public ArrayList<Node> steps = null;
	
	public Plan() {
		this.steps = new ArrayList<>();
	}
	
	public Plan(ArrayList<Node> x) {
		this.steps = x;
	}
	
	public void add(Node n) {
		this.steps.add(n);
		this.updateCostScore();
	}
	
	public void merge(Plan p) {
		if (p != null) {    // Plan can be empty
			for (Node n : p.steps) {
				this.add(n);
			}
		}
	}
	
	private void updateCostScore() {
		long totalScore = 0;
		int totalCost = 0;
		
		for (int i = this.steps.size()-1; i > 0; i--) {
			Node start = this.steps.get(i);
			Node end = this.steps.get(i-1);
			totalCost += Util.getInstance().absDistance(start.m, end.m);
		}
		
		int stationCount = 0;
		for (Node n : this.steps) {
			if (n.cell instanceof Station) {
				Station s = (uk.ac.nott.cs.g54dia.multilibrary.Station)n.cell;
				totalScore += s.getTask().getRequired();
				stationCount++;
			}
		}
		
		this.cost = totalCost;
		this.score = totalScore * stationCount;
	}
	
	public int getNumberOfTasks() {
		int number = 0;
		for (Node n : this.steps) {
			if (n.cell instanceof Station)
				number++;
		}
		return number;
	}

	@Override
	public int compareTo(Plan p) {
		return (int) (this.score - p.score);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Node n : this.steps) {
			sb.append(n);
		}
		
		sb.append("; Score:" + this.score + ", Cost:" + this.cost);
		
		return sb.toString();
	}
}
