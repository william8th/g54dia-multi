package uk.ac.nott.cs.g54dia.multilibrary;
import java.util.Random;

/*
 * Author: William Heng <william@williamheng.com>
 * Date: 10th of February 2015
 */

public class RandomGenerator {

	private static RandomGenerator instance = null;
	
	private Random r;
	
	protected RandomGenerator() {
		r = new Random();
	}
	
	public static RandomGenerator getInstance() {
		if (instance == null) {
			instance = new RandomGenerator();
		}
		
		return instance;
	}
	
	public synchronized void setSeed(int seed) {
		r = new Random();
		r.setSeed(seed);
	}
	
	public double nextDouble() {
		return r.nextDouble();
	}
	
}